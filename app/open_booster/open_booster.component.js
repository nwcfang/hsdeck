/**
 * Created by Mikhail Gusev on 07.12.16.
 */

(function () {
    'use strict';

    angular
        .module('openBooster')
        .component('openBooster', {
            templateUrl: 'open_booster/open_booster.template.html',
            controller: OpenBoosterController
        });

    function OpenBoosterController($http, saveBooster) {
        var vm = this;
        vm.title = 'OpenBoosterController';
        vm.boosters = null;

        activate();

        vm.addValue = function (value, whichOne){
            var newValue = vm[whichOne] + value;

            if ((newValue >= 0) && (newValue <= 5)) {
                vm[whichOne] = newValue;
            }
        };

        vm.addBooster = function () {
            saveBooster.save(vm.common, vm.rare, vm.epic, vm.legend);
            activate();
        };

        vm.isAllCardAssigned = function () {
            const MAX_CARD = 5;

            return (vm.common + vm.rare + vm.epic + vm.legend) >= MAX_CARD;
        };

        vm.canReduceCards = function (cardsType) {
            return vm[cardsType] <= 0;
        };

        function activate() {
            $http.get("/api/booster_results").then(function(response){
                vm.boosters = response.data;
            });
            vm.common = 0;
            vm.rare = 0;
            vm.epic = 0;
            vm.legend = 0;
        }
    }


})();


