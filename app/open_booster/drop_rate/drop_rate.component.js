/**
 * Created by Mikhail Gusev on 07.12.16.
 */

(function () {
    'use strict';

    angular
        .module('openBooster')
        .component('dropRate', {
            templateUrl: 'open_booster/drop_rate/drop_rate.template.html',
            controller: dropRateController,
            bindings: {
                boosters: '<'
            }
        });

    dropRateController.$inject = [];

    /* @ngInject */
    function dropRateController() {
        var vm = this;
        vm.title = 'dropRateController';
        vm.rates = {
            common: null,
            rare: null,
            epic: null,
            legend: null
        };
        vm.$onChanges = onChanges;

        function activate() {
            if (vm.boosters) {
                vm.commonRate = countDropRate('common');
                vm.rareRate = countDropRate('rare');
                vm.epicRate = countDropRate('epic');
                vm.legendRate = countDropRate('legend');
            }
        }

        function countDropRate() {
            var allCardsCount = vm.boosters.length * 5,
                commonCount = null,
                rareCount = null,
                epicCount = null,
                legendCount = null;

            for ( var i = 0; i < vm.boosters.length; i++) {
                commonCount += vm.boosters[i].common;
                rareCount += vm.boosters[i].rare;
                epicCount += vm.boosters[i].epic;
                legendCount += vm.boosters[i].legend;

            }

            vm.rates.common = calcRate(commonCount, allCardsCount);
            vm.rates.rare = calcRate(rareCount, allCardsCount);
            vm.rates.epic = calcRate(epicCount, allCardsCount);
            vm.rates.legend = calcRate(legendCount, allCardsCount);

            function calcRate (oneTypeCount, total) {
                return ((oneTypeCount * 100) / total).toFixed(2);
            }
        }

        function onChanges () {
            activate();
        }
    }

})();

