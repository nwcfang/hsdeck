/**
 * Created by Mikhail Gusev on 21.10.16.
 */

(function () {
    'use strict';

    angular
        .module('countPanel')
        .component('countPanel', {
            templateUrl: 'count_panel/count_panel.template.html',
            controller: CountPanelController
        });

    CountPanelController.$inject = [];

    /* @ngInject */
    function CountPanelController() {
        var vm = this;
        vm.title = 'CountPanelController';
        vm.spells = new Spells(0);
        vm.minions = new Minions(0);
        vm.weapons = 0;

        activate();

        ////////////////

        function activate() {

        }

        function Minions(count) {
            this.count = count;
            this.setCount = function(value) {
                this.count = value;
            }
        }

        function Spells(count) {
            this.count = count;
            this.setCount = function(value) {
                this.count = value;
            }
        }
    }
})();



