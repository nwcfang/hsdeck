/**
 * Created by Mikhail Gusev on 21.10.16.
 */

(function () {
    'use strict';

    angular
        .module('cardView')
        .component('cardView', {
            templateUrl: 'card_view/card_view.template.html',
            controller: CardViewController,
            binding: '='
        });

    CardViewController.$inject = [];

    /* @ngInject */
    function CardViewController() {
        var vm = this;
        vm.title = 'CardViewController';

        vm.cards = [
            {
                'image': "image/hs-images/8.png"
            },
            {
                'image': "image/hs-images/9.png"
            },
            {
                'image': "image/hs-images/22.png"
            },
            {
                'image': "image/hs-images/23.png"
            },
            {
                'image': "image/hs-images/28.png"
            },
            {
                'image': "image/hs-images/30.png"
            },
            {
                'image': "image/hs-images/34.png"
            },
            {
                'image': "image/hs-images/36.png"
            },
            {
                'image': "image/hs-images/37.png"
            },
            {
                'image': "image/hs-images/41.png"
            },
            {
                'image': "image/hs-images/51.png"
            },
            {
                'image': "image/hs-images/61.png"
            },
            {
                'image': "image/hs-images/64.png"
            },
            {
                'image': "image/hs-images/67.png"
            },
            {
                'image': "image/hs-images/68.png"
            }
        ];



        activate();

        ////////////////

        function activate() {

        }
    }
})();


