/**
 * Created by Mikhail Gusev on 10.12.16.
 */

(function () {
    'use strict';

    angular
        .module('services')
        .factory('saveBooster', saveBooster);

    saveBooster.$inject = ['$resource'];

    /* @ngInject */
    function saveBooster($resource) {
        var service = {
            save: save
        };
        return service;

        ////////////////

        function save(common, rare, epic, legend) {
            var Booster = $resource('/api/booster_results/:boosterId', {boosterId:'@id'});
            var booster = new Booster({common: common, rare: rare, epic: epic, legend: legend});
            console.log(common);
            console.log(rare);
            console.log(epic);
            console.log(legend);
            console.log(booster);
            booster.$save();
        }
    }

})();


