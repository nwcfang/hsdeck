/**
 * Created by Mikhail Gusev on 10.12.16.
 */

(function () {
    'use strict';

    angular
        .module('services')
        .factory('getBoosters', getBoosters);

    getBoosters.$inject = ['$resource'];

    /* @ngInject */
    function getBoosters($resource) {
        var service = {
            get_boosters: get_boosters
        };
        return service;

        ////////////////

        function get_boosters() {
            var Boosters = $resource('/boosters');
            return Boosters.query()
        }
    }

})();




