/**
 * Created by Mikhail Gusev on 21.10.16.
 */


(function () {
    'use strict';

    angular
        .module('hsDeck')
        .controller('mainController', mainController);

    mainController.$inject = [];

    function mainController() {
        var vm = this;
        vm.title = 'mainController';

        vm.test = "Hello! My name is Angular.";

        activate();

        ////////////////

        function activate() {

        }
    }

})();

